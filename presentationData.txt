﻿                 77777                  
               777777777                
              777777777777              
            7###77777777777             
          7777###777777777777          
         7777777#####777777777          
       7777777777#####7777777777        
     777777777777#####777777777777      
    77777777777777######77777777777     
  77777777777777777##7###777777777777   
 777777777777777777##777#####777777777  
7777777777777777777##7777######777777777
7777777777777777777##7777######777777777
 777777777777777777##77777####77777777  
  77777777777777777##7777777777777777   
    777777777777777##77777777777777     
     777777777777#####777777777777      
       7777777777######777777777        
         77777777#####77777777          
          7777777777777777777           
            777777777777777             
             7777777777777              
               777777777                
                 77777                  
                  777                   
//NEWSLIDE
Od zera do Git usera, czyli jak zapomnieć o TfuFSie

Piotr Westfalewicz
piotrwest@gmail.com
//NEWSLIDE
Agenda:

-co to git? Krótkie porównanie z innymi VCS
-podstawy pracy w gicie - "pisanie do szuflady"
-tips & tricks
-lekarstwo na TfuFS, czyli git-tfs
-jak najszybiej rozpocząć współpracę z git i TfuFS
-codzienne użytkowanie git oraz TfuFS (różne workflow)
-narzędzia
-możliwości gita, najbardziej przydatne komendy
-krótko o git internals
//NEWCHAPTER
CO TO GIT?
//NEWSLIDE
Git to bezpłatny, open-sourcowy, rozproszony
system kontroli wersji. Autorem jest Linus.

Dla dużych (Facebook) jak i małych graczy (ja;)).

Ciekawostka: git jest trzymany w gicie!
https://github.com/git/git
//NEWSLIDE
Google            PostgreSQL
Facebook          Android
Microsoft         Linux
twitter           Rails
LinkedIn          Qt
Netflix           Gnome
//NEWSLIDE
ROZPROSZONY:

Każdy użytkownik ma pełną kopię repozytorium.
Praktycznie każdy workflow jest możliwy do realizacji.
    Subversion-Style Workflow
    Integration Manager Workflow
    Dictator and Lieutenants Workflow
[images]
//NEWSLIDE
PORÓWNANIE Z INNYMI VCS:
//NEWSLIDE
Rozproszony->wszystko lokalnie->szybki!

GIT vs SVN
Operation            times faster than SVN
Commit Files                   4x
Commit Images                  4x
Diff                           16x
Diff Tags                      71x
Log (50)                       31x
Log (All)                      325x
Log (File)                     138x
Update                         3x
Blame                          1x

Nieprzyjemne doświadczenia z SVN:
-wolny
-wystąpił błąd
-branch-merge wtf
-po co komu działający serwer?
//NEWSLIDE
GIT vs Mercurial
Względem możliwości - podobny do hg.
//NEWSLIDE
GIT vs TFS...
Bez porównania z TFSem jako systemem kontroli wersji...

Nie trzeba nic "checkoutować" (TFS śledzi pliki po atrybutach read-only).
TF11 w "Local Workspaces" śledzi pliki po czasie modyfikacji, rozmiarze pliku i zcachowanej kopii lokalnej...
Jakiekolwiek operacje w TFS poza commit/ammend/view history to bolączka.
Brak exclusive locks..
Branch w gicie nabiera nowego znaczenia.
Dużo, dużo więcej...
//NEWCHAPTER
Git INTRO
//NEWSLIDE
Jak git przechowuje informacje?

Niektóre z systemów (CVS, Subversion, Perforce, Bazaar):
-------------------TIME----------------------->
V1        V2        V3        V4        V5
file A--->d1----------------->d2
file B----------------------->d1------->d2
file C--->d1------->d2----------------->d3

Git:
-------------------TIME----------------------->
V1        V2        V3        V4        V5
file A1-->A1------->A1------->A2------->A2
file B1-->B1------->B1------->B2------->B3
file C1-->C2------->C3------->C3------->C4
//NEWSLIDE
Spójność danych zapewniona przez filozofię gita:
z wszystkiego jest wyliczana suma kontrolna, a następne
obiekty odwołują się po tych sumach.

SHA-1 hash (40 chars):
24b9da6552252987aa493b52f8696cd6d3b00373

Pliki zapisane po hashu, nie po nazwie plików.
//NEWSLIDE
Git praktycznie tylko zapisuje dane, nigdy nie usuwa.

Są wyjątki!
git filter-branch --force --index-filter \
  'git rm --cached --ignore-unmatch Rakefile' \
  --prune-empty --tag-name-filter cat -- --all
git reflog expire --expire=now --all
git gc --prune=now
git gc --aggressive --prune=now
//NEWSLIDE
Trójstanowy git:
WORKSPACE       INDEX       LOCAL REPOSITORY
    |             |                |
    |<----------------checkout-----|
    |             |                |
    |             |                |
    |----stage--->|                |
    |             |                |
    |             |                |
    |             |----commit----->|
    |             |                |

Dodatkowo STASH.
Ściąga: http://ndpsoftware.com/git-cheatsheet.html
//NEWSLIDE
Git example repo:

C:\0Cloud\OSS\fluent-nhibernate
//NEWSLIDE
Random....

DAG(directed acyclic graph)
octopus
    [gitextreeme:
        g checkout --orphan
		g cm 'x' --allow-empty
		echo HASH_OF_ROOT HASH_OF_PARENT1 HASH_OF_PARENT2 HASH_OF_PARENT3 >> .git/info/grafts]
		
C:\GitPresentation\gitextreeme
//NEWCHAPTER
Setup
//NEWSLIDE
instalacja:
uruchomić exe: http://msysgit.github.io/
Open git bash here - checked!
autocrlf = false - checked!
add git to PATH - checked!
//NEWSLIDE
konfig do gita (użytkownika) trzymany w %USERPROFILE%
Warto ustawić:
//NEWSLIDE
git config --global user.name "user"
git config --global user.email user@the.best.com
//NEWSLIDE
git config --global core.editor "'C:\Program Files (x86)\Notepad++\notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
//NEWSLIDE
git config --global merge.tool kdiff3
git config --global mergetool.kdiff3.path "C:\Program Files (x86)\KDiff3\kdiff3.exe"
git config --global mergetool.kdiff3.keepBackup false
git config --global mergetool.kdiff3.trustExitCode false

KDiff3 -> Settings -> Directory -> Backup files (.orig) uncheck
//NEWSLIDE
Więcej opcji na przyszłość:
//Allow all Git commands to use colored output, if possible
git config --global color.ui auto
//Disable the advice shown by Git when you attempt to push something that’s not fast forward-able
git config --global advice.pushNonFastForward false
//Disable “how to stage/unstage/add” hints given by git status:
git config --global advice.statusHints false
//Tell Git which whitespace problems it should recognize, namely any whitespace at the end of a line, as well as mixed spaces and tabs: See the man page for more possible options on this.
git config --global core.whitespace trailing-space,space-before-tab
//Allow git diff to do basic rename and copy detection:
git config --global diff.renames copies
//Always show a diffstat at the end of a merge:
git config --global merge.stat true
//NEWSLIDE
Mój gitconfig:
git config --global --edit

Uwaga na modyfikacje programów trzecich pliku gitconfig!
//NEWCHAPTER
Podstawy pracy w gicie - "pisanie do szuflady"
//NEWSLIDE
Demo.

git init
git commit --allow-empty (trick: gitk, log)
git add (-A, --)
git rm (--cached)
https://github.com/github/gitignore/blob/master/VisualStudio.gitignore
./git/info/exclude
gitk, git gui
git commit -a
git log --pretty=oneline
//NEWSLIDE
Demo.

Fajniejszy przykład + VS.
C:\0Cloud\OSS\fluent-nhibernate
//NEWSLIDE
Branching && merging
git checkout (-b)
git branch (-d)
git merge
git rebase (-i)
//NEWCHAPTER
Tips & tricks
//NEWSLIDE
Git, git, git, git, git, git, git, git, git,...
G, g, g, g, g, g, g, g,... 

Edycja pliku w notepad++
e (w .bashrc)

%USERPROFILE% .bashrc
//NEWSLIDE
git clean, xdfn, e

Demo.
C:\GitPresentation\GitPresentationDemo
//NEWSLIDE
Aliases...
//NEWSLIDE
gitk z ścieżką
gk
gk -- NLog/src/NLog/
gk -- NLog/src/NLog/NLog.netfx40.csproj
//NEWSLIDE
git <tab><tab>
git rebase <tab>
//NEWSLIDE
Składanie komend, &&, $().
Ścieżka do pliku z g f.

Demo.
C:\0Cloud\OSS\fluent-nhibernate
//NEWSLIDE
Gdy wpisywanie 40 znaków SHA-1 jest nie za wygodne...

revisions

Demo.
C:\Wcześniejsze basic demo repo
//NEWSLIDE
Powershell?

poshgit.
//NEWSLIDE
http://gitready.com/
//NEWCHAPTER
Make love not war: git-tfs
//NEWSLIDE
Co to... git-tf?

Ciekawostka: napisany przez MS w JAVIE
Wolny...
Kiepski...
Błędnogenny...
http://visualstudiogallery.msdn.microsoft.com/abafc7d6-dcaa-40f4-8a5e-d6724bdb980c (ocena: 3/5)
//NEWSLIDE
TFS11 local workspaces...

hahahahhahahahahah....

Brian Harry o "Version Control Model Enhancements in TFS 11":
"(...) we’ve dramatically improved the offline scenario (...)"
[http://blogs.msdn.com/b/bharry/archive/2011/08/02/version-control-model-enhancements-in-tfs-11.aspx]

um, yeah.
//NEWSLIDE
git

local:
   code
   branching
   commits
   history
   everything
//NEWSLIDE
TFS
   code
Brian Harry:
"You won’t get big long hangs – but rather nice error messages that tell you you need to be online."
//NEWSLIDE
git > TFS
//NEWSLIDE
Setup
https://github.com/git-tfs/git-tfs/downloads
add git-tfs to PATH - checked!
git tfs clone https://piotrwest.visualstudio.com/defaultcollection $/GitPresentationDemo
open solution without internet - checked!
gitignore - checked!
//NEWSLIDE
Demo.
Work... work... work...
git tfs ct (do not touch)
git tfs shelve
//NEWSLIDE
Demo.
Commit
Merge
//NEWSLIDE
Workflow 1 (jeżeli robisz brancha, wiedz, że coś się dzieje):
git tfs pull
git checkout -b dev
git commit, commit
git merge master
git tfs ct
//NEWSLIDE
Workflow 2 (leniwy i brzydki):
git tfs pull
git commit, commit
git tfs pull
git tfs ct
//NEWSLIDE
Workflow 1 (leniwy i ładny):
git tfs fetch
git commit, commit, commit
git tfs fetch
git rebase
git tfs ct
//NEWSLIDE
git tfs rcheckin -w x:resolve
Yes.
I am a robot.
Next question?
//NEWSLIDE
Problemy?

    automatic merge - failed 1 time
    unshelve z polskimi znakami
    dodawanie projektów
//NEWCHAPTER
Narzędzia
//NEWSLIDE
Powiedzieliśmy już o:
msysgit, git-tfs, git-tf
//NEWSLIDE
SourceTree
//NEWSLIDE
TortoiseGit
//NEWSLIDE
Git Source Control Provider
//NEWSLIDE
GitExtensions
//NEWSLIDE
Co wybrać?
//NEWCHAPTER
Najbardziej przydatne komendy
//NEWSLIDE
When shit happens....
git reflog
(defaults:
--expire=90
--expire-unreachable=30)
//NEWSLIDE
git diff

WORKSPACE       INDEX       LOCAL REPOSITORY
    |             |                |
    |----------------diff HEAD---->|
    |             |                |
    |             |                |
    |----diff---->|                |
    |             |                |
    |             |                |
    |             |-diff --cached->|
    |             |                |

//NEWSLIDE
Aliasowo:

WORKSPACE       INDEX       LOCAL REPOSITORY
    |             |                |
    |---------------------ac------>|
    |             |                |
    |             |                |
    |----ntbc---->|                |
    |             |                |
    |             |                |
    |             |-------tbc----->|
    |             |                |
//NEWSLIDE
git reset
//NEWSLIDE
GIT REPO

                                       HEAD, master
                                            |
                                            v
  8d3djkn            ijd92ds             aoiw3ed
v1 file.txt <----- v2 file.txt  <----- v3.file.txt


 WORKSPACE            INDEX               HEAD
v3 file.txt        v3 file.txt         v3 file.txt
//NEWSLIDE
git reset --soft HEAD^

                   HEAD, master
                        |
                        v
  8d3djkn            ijd92ds             aoiw3ed
v1 file.txt <----- v2 file.txt  <----- v3.file.txt


 WORKSPACE            INDEX               HEAD
v3 file.txt        v3 file.txt         v2 file.txt
//NEWSLIDE
git reset [--mixed] HEAD^

                   HEAD, master
                        |
                        v
  8d3djkn            ijd92ds             aoiw3ed
v1 file.txt <----- v2 file.txt  <----- v3.file.txt


 WORKSPACE            INDEX               HEAD
v3 file.txt        v2 file.txt         v2 file.txt
//NEWSLIDE
git reset --hard HEAD^

                   HEAD, master
                        |
                        v
  8d3djkn            ijd92ds             aoiw3ed
v1 file.txt <----- v2 file.txt  <----- v3.file.txt


 WORKSPACE            INDEX               HEAD
v2 file.txt        v2 file.txt         v2 file.txt
//NEWSLIDE
aliases +
 bisect
 reflog
 revert
 git checkout HEAD^ path/to/file
//NEWSLIDE
Możliwości gita
//NEWSLIDE
cherry-pick
//NEWSLIDE
rebase + revisions selecting
//NEWSLIDE
filter-branch
//NEWSLIDE
submodules (add, !remove)
//NEWSLIDE
With great power comes great responsibility...

more than bad merge... evil merge (w TFSie też można go zrobić)
rebase na pushed branchu
octopus
//NEWSLIDE
Prawie zawsze można zrealizować coś na więcej niż jeden sposób.
//NEWSLIDE
Demo.

Cel: chemy dodać jakąś zmianę do naszego ostatniego commita.
 R1. commit --ammend
 R2. reset --soft, commit
 R3. commit, rebase, squash
//NEWSLIDE
Getting PRO!

gitk master --not $( git show-ref --heads | cut -d' ' -f2 |
                                grep -v '^refs/heads/master' )
//NEWCHAPTER
Git internals

71 strona
C:\GitPresentation\git-presentations\railsconf08\getting_git.pdf
[https://github.com/schacon/git-presentations]
//NEWCHAPTER
Pytania?

Czy w gicie można... 
TAK.
//NEWSLIDE
Źródła:
1. http://git-scm.com/
2. https://github.com/schacon/git-presentations
3. https://help.github.com/articles/remove-sensitive-data
4. http://ndpsoftware.com/git-cheatsheet.html
5. https://github.com/jagregory/fluent-nhibernate