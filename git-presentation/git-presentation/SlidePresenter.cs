﻿using System;
using System.Collections.Generic;

namespace GitPresentation
{
    public class SlidePresenter
    {
        private readonly IDataProvider provider;
        private string currentData;

        public SlidePresenter(IDataProvider provider)
        {
            this.provider = provider;
        }

        public void Start()
        {
            string data;
            string hint;
            var result = provider.ReadData(out data, out hint);
            if (!result)
            {
                Console.WriteLine(hint);
                return;
            }
            currentData = data;
            DisplayData(data);

            EventLoop();
        }

        private static readonly HashSet<ConsoleKey> AllowedKeys = new HashSet<ConsoleKey>
            {
                ConsoleKey.RightArrow, ConsoleKey.LeftArrow,
                ConsoleKey.UpArrow, ConsoleKey.DownArrow
            };

        private void EventLoop()
        {
            ConsoleKeyInfo keyInfo;
            while (AllowedKeys.Contains((keyInfo = Console.ReadKey(true)).Key))
            {
                string data, hint;
                switch (keyInfo.Key)
                {
                    case ConsoleKey.LeftArrow:
                        if (!provider.PreviousSlide(out data, out hint))
                        {
                            DisplayFailure(hint, currentData);
                            break;
                        }
                        DisplayData(data);
                        break;
                    case ConsoleKey.UpArrow:
                        if (!provider.NextChapter(out data, out hint))
                        {
                            DisplayFailure(hint, currentData);
                            break;
                        }
                        DisplayData(data);
                        break;
                    case ConsoleKey.RightArrow:
                        if (!provider.NextSlide(out data, out hint))
                        {
                            DisplayFailure(hint, currentData);
                            break;
                        }
                        DisplayData(data);
                        break;
                    case ConsoleKey.DownArrow:
                        if (!provider.PreviousChapter(out data, out hint))
                        {
                            DisplayFailure(hint, currentData);
                            break;
                        }
                        DisplayData(data);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void DisplayData(string data)
        {
            currentData = data;
            //Console.Clear();
            Console.Write(provider.GetPositionInfo());
            //Console.WriteLine();
            Console.WriteLine();
            Console.Write(data);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }

        private void DisplayFailure(string hint, string previousData)
        {
            //Console.Clear();
            Console.Write(provider.GetPositionInfo());
            Console.WriteLine(hint);
            //Console.WriteLine();
            Console.Write(previousData);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}