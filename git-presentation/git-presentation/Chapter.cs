﻿using System.Collections.Generic;
using System.Linq;

namespace GitPresentation
{
    public class Chapter
    {
        public Chapter(IEnumerable<string> slides)
        {
            Slides = new List<Slide>(slides.Select(str => new Slide(str)));
        }
        public List<Slide> Slides { get; set; } 
    }
}