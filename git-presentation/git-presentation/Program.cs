﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace GitPresentation
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Contains("-d"))
            {
                var modifiedArgs = args.ToList();
                modifiedArgs.Remove("-d");
                args = modifiedArgs.ToArray();
                Debugger.Launch();
            }

            var argsAreValid = ValidateArgs(args);
            if (!argsAreValid)
            {
                Usage();
                return;
            }
            int chapter, slide;
            var chapterParsingSucceeded = int.TryParse(args[0], out chapter);
            if (!chapterParsingSucceeded)
            {
                Console.WriteLine("'{0}' is not a valid (chapter) number.", args[0]);
                return;
            }
            var slideParsingSucceeded = int.TryParse(args[1], out slide);
            if (!slideParsingSucceeded)
            {
                Console.WriteLine("'{0}' is not a valid (slide) number.", args[1]);
                return;
            }

            SetUtf8Encoding();

            var provider = new SingleFileDataProvider(AppDomain.CurrentDomain.BaseDirectory, chapter, slide);
            new SlidePresenter(provider).Start();
        }

        private static void SetUtf8Encoding()
        {
            try
            {
                // Set encoding using endianness of this system. 
                // We're interested in displaying individual Char objects, so  
                // we don't want a Unicode BOM or exceptions to be thrown on 
                // invalid Char values.
                Console.OutputEncoding = new UnicodeEncoding(!BitConverter.IsLittleEndian, false);
            }
            catch (IOException)
            {
                Console.OutputEncoding = new UTF8Encoding();
            }
        }

        private static bool ValidateArgs(string[] args)
        {
            return args.Length == 2;
        }

        private static void Usage()
        {
            Console.WriteLine("Usage: git presentation <chapter> <slide> (-d - for debugging)");
        }
    }
}