﻿using System.Collections.Generic;

namespace GitPresentation
{
    public class Presentation
    {
        public Presentation()
        {
            Chapters = new List<Chapter>();
        }

        public List<Chapter> Chapters { get; set; }

        public bool HasChapter(int chapter)
        {
            return chapter > 0 && Chapters.Count >= chapter;
        }

        public bool HasSlide(int chapter, int slide)
        {
            return HasChapter(chapter) && slide > 0 && Chapters[chapter - 1].Slides.Count >= slide;
        }

        public string GetSlide(int chapter, int slide)
        {
            return Chapters[chapter - 1].Slides[slide - 1].Data;
        }
    }
}