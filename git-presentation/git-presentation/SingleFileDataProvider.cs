﻿using System;
using System.IO;
using System.Linq;

namespace GitPresentation
{
    public class SingleFileDataProvider : DataProviderBase, IDataProvider
    {
        private const string slideSeparator = "//NEWSLIDE\r\n";
        private const string chapterSeparator = "//NEWCHAPTER\r\n";
        private Presentation presentation;

        public SingleFileDataProvider(string lookupDir, int currentChapter, int currentSlide)
            : base(lookupDir, currentChapter, currentSlide)
        {
            ConstructPresentation();
        }

        private void ConstructPresentation()
        {
            var txtFile = Directory
                .EnumerateFiles(LookupDir, "*.txt")
                .Select(sr => new FileInfo(sr))
                .Single(fi => fi.Name.Contains("presentation"));
            var wholePresentation = File.ReadAllText(txtFile.FullName);
            presentation = new Presentation();
            var chapters = wholePresentation.Split(new[] {chapterSeparator}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var chapter in chapters)
            {
                var slides = chapter.Split(new[] {slideSeparator}, StringSplitOptions.RemoveEmptyEntries);
                presentation.Chapters.Add(new Chapter(slides));
            }
        }

        public override bool ReadData(out string data, out string hint)
        {
            data = null;
            if (presentation.HasChapter(CurrentChapter))
            {
                return ReadSlide(out data, out hint);
            }
            hint = ConstructHint(presentation.Chapters.Count, "chapter");
            return false;
        }

        private string ConstructHint(int maxItemCount, string itemName)
        {
            return string.Format("Couldn't read '{0}' {1}. Choose something from 1 to {2}.", CurrentChapter, itemName, maxItemCount);
        }

        private bool ReadSlide(out string data, out string hint)
        {
            hint = data = null;
            if (presentation.HasSlide(CurrentChapter, CurrentSlide))
            {
                data = presentation.GetSlide(CurrentChapter, CurrentSlide);
                return true;
            }
            hint = ConstructHint(presentation.Chapters[CurrentChapter - 1].Slides.Count, "slide");
            return false;
        }

        protected override int GetMaxSlideFromCurrentChapter()
        {
            var chapterIndex = CurrentChapter - 1;
            if (chapterIndex < 0)
                return 0;
            if (CurrentChapter > presentation.Chapters.Count)
                return 0;
            return presentation.Chapters[chapterIndex].Slides.Count;
        }
    }
}