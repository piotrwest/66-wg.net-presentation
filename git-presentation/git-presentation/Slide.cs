﻿namespace GitPresentation
{
    public class Slide
    {
        public Slide(string data)
        {
            Data = data;
        }

        public string Data { get; set; } 
    }
}