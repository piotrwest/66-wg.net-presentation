﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace GitPresentation
{
    public class DirDataProvider : DataProviderBase, IDataProvider
    {
        public DirDataProvider(string lookupDir, int currentChapter, int currentSlide)
            : base(lookupDir, currentChapter, currentSlide)
        {
        }

        public override bool ReadData(out string data, out string hint)
        {
            data = null;
            var dirsInLookupDir = Directory.EnumerateDirectories(LookupDir).Select(p => new DirectoryInfo(p).Name);
            var ints = ConvertItemsToInts(dirsInLookupDir);
            if (ints.Contains(CurrentChapter))
            {
                return ReadSlides(out data, out hint);
            }
            ConstructHint(out hint, ints, "chapter");
            return false;
        }

        private bool ReadSlides(out string data, out string hint)
        {
            hint = data = null;
            var slides = GetParsedSlidesList();
            if (slides.Contains(CurrentSlide))
            {
                data = ReadDataFromSlide();
                return true;
            }
            ConstructHint(out hint, slides, "slide");
            return false;
        }

        private List<int> GetParsedSlidesList()
        {
            var pathToChapter = Path.Combine(LookupDir, CurrentChapter.ToString(CultureInfo.InvariantCulture));
            if (!Directory.Exists(pathToChapter))
                return new List<int>();
            var slidesRaw = Directory.EnumerateFiles(pathToChapter, "*.txt").Select(sr => new FileInfo(sr).Name.Replace(".txt", ""));
            var slides = ConvertItemsToInts(slidesRaw);
            return slides;
        }

        private string ReadDataFromSlide()
        {
            var fo = File.ReadAllText(
                Path.Combine(LookupDir,
                             CurrentChapter.ToString(CultureInfo.InvariantCulture),
                             CurrentSlide.ToString(CultureInfo.InvariantCulture) + ".txt"), Encoding.UTF8);
            return fo;
        }

        private void ConstructHint(out string hint, List<int> ints, string itemName)
        {
            ints.Sort();
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("Couldn't read '{0}' {1}. Choose something from this:", CurrentChapter, itemName));
            var howMuchPrinted = 0;
            foreach (var intItem in ints)
            {
                sb.Append(intItem.ToString(CultureInfo.InvariantCulture) + "\t");

                howMuchPrinted++;
                if (howMuchPrinted >= 10)
                {
                    sb.AppendLine();
                    howMuchPrinted = 0;
                }
            }
            hint = sb.ToString();
        }

        private static List<int> ConvertItemsToInts(IEnumerable<string> rawStrings)
        {
            var dirsAsInts = new List<int>();
            foreach (var dir in rawStrings)
            {
                int dirAsInt;
                if (int.TryParse(dir, out dirAsInt))
                {
                    dirsAsInts.Add(dirAsInt);
                }
            }
            return dirsAsInts;
        }

        protected override int GetMaxSlideFromCurrentChapter()
        {
            var slides = GetParsedSlidesList();
            slides.Sort();
            return slides.LastOrDefault();
        }
    }
}