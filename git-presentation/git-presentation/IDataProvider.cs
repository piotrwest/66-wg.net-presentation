﻿namespace GitPresentation
{
    public interface IDataProvider
    {
        bool ReadData(out string data, out string hint);
        string GetPositionInfo();
        bool NextSlide(out string data, out string hint);
        bool PreviousSlide(out string data, out string hint);
        bool NextChapter(out string data, out string hint);
        bool PreviousChapter(out string data, out string hint);
    }
}