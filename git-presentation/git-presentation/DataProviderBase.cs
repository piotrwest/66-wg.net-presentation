﻿using System;

namespace GitPresentation
{
    public abstract class DataProviderBase
    {
        protected readonly string LookupDir;
        protected int CurrentChapter;
        protected int CurrentSlide;

        protected DataProviderBase(string lookupDir, int currentChapter, int currentSlide)
        {
            LookupDir = lookupDir;
            CurrentChapter = currentChapter;
            CurrentSlide = currentSlide;
        }

        protected bool ChangeSlideImpl(out string data, out string hint, string overflowMessage,
                                       Func<int, int> changeSlide, Func<int, int> changeChapter, Func<int> overflowSlideNumber)
        {
            var oldCurrentSlide = CurrentSlide;
            var oldCurrentChapter = CurrentChapter;
            CurrentSlide = changeSlide(CurrentSlide);
            var result = ReadData(out data, out hint);
            if (!result)
            {
                CurrentChapter = changeChapter(CurrentChapter);
                CurrentSlide = overflowSlideNumber();
                var nextResult = ReadData(out data, out hint);
                if (!nextResult)
                {
                    data = null;
                    hint = overflowMessage;
                    CurrentSlide = oldCurrentSlide;
                    CurrentChapter = oldCurrentChapter;
                    return false;
                }
                return true;
            }
            return true;
        }

        public abstract bool ReadData(out string data, out string hint);

        public string GetPositionInfo()
        {
            return string.Format("(CH:{0:00} S:{1:00}) ", CurrentChapter, CurrentSlide);
        }

        public bool NextSlide(out string data, out string hint)
        {
            return ChangeSlideImpl(out data, out hint, "The end!", s => ++s, c => ++c, () => 1);
        }

        public bool PreviousSlide(out string data, out string hint)
        {
            return ChangeSlideImpl(out data, out hint, "The beginning!",
                                   s => --s,
                                   c => --c,
                                   GetMaxSlideFromCurrentChapter
                );
        }

        private bool ChangeChapterImpl(out string data, out string hint, string overflowMessage,
                                       Func<int> changeSlide, Func<int, int> changeChapter)
        {
            var oldCurrentSlide = CurrentSlide;
            var oldCurrentChapter = CurrentChapter;
            CurrentChapter = changeChapter(CurrentChapter);
            CurrentSlide = changeSlide();
            var nextResult = ReadData(out data, out hint);
            if (!nextResult)
            {
                data = null;
                hint = overflowMessage;
                CurrentSlide = oldCurrentSlide;
                CurrentChapter = oldCurrentChapter;
                return false;
            }
            return true;
        }

        public bool NextChapter(out string data, out string hint)
        {
            return ChangeChapterImpl(out data, out hint, "This is the last chapter!", () => 1, c => ++c);
        }

        public bool PreviousChapter(out string data, out string hint)
        {
            return ChangeChapterImpl(out data, out hint, "This is the first chapter!",
                                     GetMaxSlideFromCurrentChapter,
                                     c => --c);
        }

        protected abstract int GetMaxSlideFromCurrentChapter();
    }
}