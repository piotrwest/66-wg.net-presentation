alias g='git'
alias gk='gitk'
complete -o default -o nospace -F _git g
complete -o default -o nospace -F _gitk gk
alias e='"C:\\Program Files (x86)\\Notepad++\\notepad++.exe" -multiInst -notabbar -nosession -noPlugin'